package ui;

import controller.RegistarUtilizadorController;
import java.util.Scanner;
import model.CentroExposicoes;
import model.Utilizador;

/**
 *
 * @author Ecs
 */
public class RegistarUtilizadorUI {
    
    private final RegistarUtilizadorController controller;
    public final Utilizador m_oUu;
    Scanner ler;
    
    public RegistarUtilizadorUI(CentroExposicoes ce, Utilizador u){
        controller = new RegistarUtilizadorController(ce);
        m_oUu=u;
        ler = new Scanner(System.in);
    }
    
    public void run(){
        System.out.println("Registar Utilizador:");

        do{
            controller.novoUtilizador();
            introduzDados();
            
            System.out.print("continuar (s/n)?");
        }while ("s".equalsIgnoreCase(ler.nextLine()));      
    }
    
    public void introduzDados(){

        String nome, email, username, password;
        System.out.print("nome    :"); nome = ler.nextLine();
        System.out.print("email   :"); email = ler.nextLine();
        System.out.print("username:"); username = ler.nextLine();
        System.out.print("password:"); password = ler.nextLine();

        if(controller.setUtilizador(nome, email, username, password)){
            System.out.println(controller.getUtilizadorString());
            System.out.print("confirma (s/n)?");
            
            if("s".equalsIgnoreCase(ler.nextLine())){
                if( controller.registaUtilizador())
                    System.out.println("Sucesso");
                else
                    System.out.println("ERRO: falhou validacao global");
            }
        }
        else
            System.out.println("ERRO: falhou validacao local");
    }
}
