/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import controller.DefinirFAEController;
import model.CentroExposicoes;
import model.Exposicao;
import model.FAE;
import model.Utilizador;
import utils.Utils;
import java.util.List;

/**
 *
 * @author rosamarianascimentodasilva
 */
public class DefinirFAEUI
{

    private final CentroExposicoes m_centro_exposicoes;
    private final DefinirFAEController m_controllerFAE;

    public DefinirFAEUI(CentroExposicoes centroexposicoes)
    {
        m_centro_exposicoes = centroexposicoes;
        m_controllerFAE = new DefinirFAEController(m_centro_exposicoes);

    }

    public void run()
    {
        String strIdOrganizador = introduzIdOrganizador();

        List<Exposicao> le = m_controllerFAE.getExposicoesOrganizador(strIdOrganizador);

        apresentaExposicoesOrganizador(strIdOrganizador, le);

        Exposicao e = selecionaExposicao(le);
        
        if (e != null)
        {
            m_controllerFAE.selectExposicao(e);

            List<Utilizador> lu = m_controllerFAE.getUtilizadores();
            
            Utilizador u;
            do
            {
                apresentaUtilizadores(lu);

                u = selecionaUtilizador(lu);

                if (u!= null)
                {
                    FAE fae = m_controllerFAE.addMembroFAE(u.getID());
                    
                    boolean conf = Utils.confirma("Confirma o FAE:" + fae.toString() + "(S/N)?");
                    if (conf)
                    {
                        m_controllerFAE.registaMembroFAE(fae);
                        
                        // Não apresenta o utilizador que já foi convertido em FAE
                        lu.remove(u);
                    }
                    
                }
            }while (u!=null);
        } 
        else
        {
            System.out.println("Criação dos Membros do FAE cancelada.");
        }
    }

    private String introduzIdOrganizador()
    {
        return Utils.readLineFromConsole("Introduza Id Organizador: ");
    }

    private void apresentaExposicoesOrganizador(String strIdOrganizador, List<Exposicao> le)
    {
        System.out.println("Exposicoes do organizador " + strIdOrganizador + ":");

        int index = 0;
        for (Exposicao e : le)
        {
            index++;

            System.out.println(index + ". " + e.toString());
        }
        System.out.println("");
        System.out.println("0 - Cancelar");
    }

    private void apresentaUtilizadores(List<Utilizador> lu)
    {
        System.out.println("Utilizadores" + ":");

        int index = 0;
        for (Utilizador u : lu)
        {
            index++;

            System.out.println(index + ". " + u.toString());
        }
        System.out.println("");
        System.out.println("0 - Cancelar");
    }

    private Exposicao selecionaExposicao(List<Exposicao> le)
    {
        String opcao;
        int nOpcao;
        do
        {
            opcao = Utils.readLineFromConsole("Introduza opção: ");
            nOpcao = new Integer(opcao);
        } while (nOpcao < 0 || nOpcao > le.size());

        if (nOpcao == 0)
        {
            return null;
        } else
        {
            return le.get(nOpcao - 1);
        }
    }

    private Utilizador selecionaUtilizador(List<Utilizador> lu)
    {
        String opcao1;
        int nOpcao1;
        do
        {
            opcao1 = Utils.readLineFromConsole("Introduza opção: ");
            nOpcao1 = new Integer(opcao1);
        } while (nOpcao1 < 0 || nOpcao1 > lu.size());

        if (nOpcao1 == 0)
        {
            return null;
        } else
        {
            return lu.get(nOpcao1 - 1);

        }
    }

}
