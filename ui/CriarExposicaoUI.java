/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ui;

import controller.CriarExposicaoController;
import model.CentroExposicoes;
import model.Utilizador;
import utils.Utils;
import static utils.Utils.apresentaLista;
import static utils.Utils.selecionaObject;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class CriarExposicaoUI
{
    private final CriarExposicaoController m_oController;
    private Utilizador m_oUu;

    public CriarExposicaoUI(CentroExposicoes ce, Utilizador u)
    {
        this.m_oController = new CriarExposicaoController(ce);
        m_oUu = u;
    }

    public void run()
    {
        System.out.println("\nCriar Exposicao:");
        m_oController.novaExposicao();

        introduzDados();
        
        List<Utilizador> lUsers = m_oController.getListaUtilizadores();
        Object obj;
        do
        {
            obj = Utils.apresentaESeleciona(lUsers, "Selecione Utilizador -> Organizador");
            if (obj != null)
                m_oController.addOrganizador((Utilizador)obj);
        } while(obj!= null);


        apresentaDados();

        if (Utils.confirma("Confirma os dados da exposição? (S/N)")) {
            if (m_oController.registaExposicao()) {
                System.out.println("Exposição criada com sucesso.");
            } else {
                System.out.println("Não foi possivel guardar corretamente a Exposição.");
            }
        }
    }
    
    private void introduzDados() {
        String sTitulo  = Utils.readLineFromConsole("Introduza Título: ");
        String sDescritivo = Utils.readLineFromConsole("Introduza Descritivo: ");
        Date oDtInicio = Utils.readDateFromConsole("Introduza Data Inicio (dd-mm-aaaa): ");
        Date oDtFim = Utils.readDateFromConsole("Introduza Data Fim (dd-mm-aaaa): ");
        String sLocal = Utils.readLineFromConsole("Introduza Local: ");
       
        m_oController.setDados(sTitulo, sDescritivo, oDtInicio, oDtFim, sLocal);
    }

    private void apresentaDados()
    {
        System.out.println("\nExposicao:\n" + m_oController.getExposicaoString());
    }
}
